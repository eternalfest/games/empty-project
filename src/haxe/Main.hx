import bugfix.Bugfix;
import debug.Debug;
import merlin.Merlin;
import game_params.GameParams;
import better_script.BetterScript;
import patchman.IPatch;
import patchman.Patchman;
import hf.Hf;
import misc.Misc;


@:build(patchman.Build.di())
class Main {
    public static function main(): Void {
        Patchman.bootstrap(Main);
    }

    public function new(
        bugfix: Bugfix,
        debug: debug.Debug,
        boss: atlas.props.Boss,
        darknessLevels: atlas.props.Darkness,
        noFireball: atlas.props.NoFireball,
        gameParams: GameParams,
        better_script: BetterScript,
        misc: Misc,
        atlas: atlas.Atlas,
        merlin: Merlin,
        patches: Array<IPatch>,
        hf: Hf
    ) {
        Patchman.patchAll(patches, hf);
    }
}
