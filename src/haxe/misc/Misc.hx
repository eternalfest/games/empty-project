package misc;

import misc.actions.CoolEvent;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import etwin.flash.Key;
import patchman.Ref;


@:build(patchman.Build.di())
class Misc {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {

        var actions = [
            (new CoolEvent(this) : IAction),
        ];

        var patches = [

        ];

        this.actions = FrozenArray.from(actions);
        this.patches = FrozenArray.from(patches);
    }

    public function coolEvent(game: hf.mode.GameMode, x: Float, y: Float, i: Int) {
        patchman.DebugConsole.log("!cool event en " + x + " " + y);
    }
}
