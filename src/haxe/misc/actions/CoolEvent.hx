package misc.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class CoolEvent implements IAction {
    public var name(default, null): String = Obfu.raw("cool_event");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var y: Float = ctx.getFloat(Obfu.raw("y"));
        var i: Int = ctx.getOptInt(Obfu.raw("i")).or(0);

        this.mod.coolEvent(game, x, y, i);

        return false;
    }
}
